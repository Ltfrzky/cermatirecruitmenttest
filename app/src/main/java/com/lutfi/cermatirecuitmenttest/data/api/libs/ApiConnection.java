package com.lutfi.cermatirecuitmenttest.data.api.libs;

import com.lutfi.cermatirecuitmenttest.BuildConfig;
import com.lutfi.cermatirecuitmenttest.data.api.model.request.SearchRequest;
import com.lutfi.cermatirecuitmenttest.data.api.model.response.SearchResponse;
import com.lutfi.cermatirecuitmenttest.util.AppConstant;

import java.util.HashMap;

import retrofit2.Call;

/**
 * Created by Lutfi on 1/24/2020.
 */
public class ApiConnection {

    private static HeaderInterceptor provideHeader() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("User-Agent", AppConstant.GITHUB_USERNAME);
        return new HeaderInterceptor(headers);
    }

    public static Call<SearchResponse> searchUser(String keyword, int page, int limit) {
        return ApiService.createService(ApiClient.class,
                OkHttpClientFactory.create(provideHeader()),
                BuildConfig.BASE_URL).searchUser(keyword, page, limit);
    }
}
