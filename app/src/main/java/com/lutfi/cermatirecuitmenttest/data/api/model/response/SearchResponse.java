package com.lutfi.cermatirecuitmenttest.data.api.model.response;

import com.google.gson.annotations.SerializedName;
import com.lutfi.cermatirecuitmenttest.data.api.model.User;

import java.util.List;

/**
 * Created by Lutfi on 1/24/2020.
 */
public class SearchResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("total_count")
    private int totalCount;
    @SerializedName("incomplete_results")
    private boolean incompleteResults;
    @SerializedName("items")
    private List<User> userList;

    public SearchResponse(String message, int totalCount, boolean incompleteResults, List<User> userList) {
        this.message = message;
        this.totalCount = totalCount;
        this.incompleteResults = incompleteResults;
        this.userList = userList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public boolean isIncompleteResults() {
        return incompleteResults;
    }

    public void setIncompleteResults(boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
