package com.lutfi.cermatirecuitmenttest.data.api.libs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Lutfi on 1/24/2020.
 */
public class ApiService {

    public ApiService() {

    }

    public static <S> S createService(Class<S> serviceClass,
                                      OkHttpClient okHttpClient,
                                      String baseUrl) {

        Gson gson = new GsonBuilder().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(serviceClass);
    }
}
