package com.lutfi.cermatirecuitmenttest.data.api.model.error;

/**
 * Created by Lutfi on 1/24/2020.
 */
public class Error {
    private String message;
    private String code;

    public Error(String message, String code) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
