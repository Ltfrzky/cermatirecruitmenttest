package com.lutfi.cermatirecuitmenttest.data.api.model.error;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lutfi on 1/24/2020.
 */
public class ApiError {
    private List<Error> errors;

    public ApiError(List<Error> errors) {
        this.errors = errors;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    @NonNull
    @Override
    public String toString() {
        return "ApiError{" +
                "message=" + errors.get(0).getMessage() +
                '}'
                ;
    }
}
