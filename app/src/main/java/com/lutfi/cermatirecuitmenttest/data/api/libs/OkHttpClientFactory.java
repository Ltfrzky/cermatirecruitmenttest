package com.lutfi.cermatirecuitmenttest.data.api.libs;

import androidx.annotation.Nullable;

import com.lutfi.cermatirecuitmenttest.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by Lutfi on 1/24/2020.
 */
public class OkHttpClientFactory {

    private static final int DEFAULT_MAX_REQUEST = 30;
    private static final int TIMEOUT = 120;

    private OkHttpClientFactory() {

    }

    public static OkHttpClient create(@Nullable HeaderInterceptor headerInterceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true);

        if (headerInterceptor != null) {
            builder.addInterceptor(headerInterceptor);
        }

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor).build();
        }

        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setMaxRequests(DEFAULT_MAX_REQUEST);
        builder.dispatcher(dispatcher);

        return builder.build();
    }

}
