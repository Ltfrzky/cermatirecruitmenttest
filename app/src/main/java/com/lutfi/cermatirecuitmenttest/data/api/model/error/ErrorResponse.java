package com.lutfi.cermatirecuitmenttest.data.api.model.error;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import okhttp3.ResponseBody;

/**
 * Created by Lutfi on 1/24/2020.
 */
public class ErrorResponse {
    @SerializedName("error")
    private ApiError error;

    public ErrorResponse(ApiError error) {
        this.error = error;
    }

    public ApiError getError() {
        return error;
    }

    public void setError(ApiError error) {
        this.error = error;
    }

    @NonNull
    @Override
    public String toString() {
        return "ErrorResponse{"+
                "error=" + error +
                '}';
    }

    public static String getErrorMessage(ResponseBody errorBody) {
        try {
            String errorResponse = errorBody.string();
            Log.d("Error","getErrorMessage(): " + errorResponse);
            return new Gson().fromJson(errorResponse, ApiError.class).getErrors().get(0).getMessage();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
