package com.lutfi.cermatirecuitmenttest.data.api.libs;

import com.lutfi.cermatirecuitmenttest.data.api.model.request.SearchRequest;
import com.lutfi.cermatirecuitmenttest.data.api.model.response.SearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Lutfi on 1/24/2020.
 */
public interface ApiClient {

    @GET("/search/users")
    Call<SearchResponse> searchUser(@Query("q") String keyword,
                                    @Query("page") int page,
                                    @Query("per_page") int limit);
}
