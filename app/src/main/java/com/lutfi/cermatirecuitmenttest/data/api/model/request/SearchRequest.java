package com.lutfi.cermatirecuitmenttest.data.api.model.request;

/**
 * Created by Lutfi on 1/24/2020.
 */
public class SearchRequest {
    private String keyword;

    public SearchRequest(String keyword) {
        this.keyword = keyword;
    }


    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
