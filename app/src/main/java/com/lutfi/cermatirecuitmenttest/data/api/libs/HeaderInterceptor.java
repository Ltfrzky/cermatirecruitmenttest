package com.lutfi.cermatirecuitmenttest.data.api.libs;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Lutfi on 1/24/2020.
 */
public class HeaderInterceptor implements Interceptor {

    private HashMap<String, String> headers;

    public HeaderInterceptor(HashMap<String, String> headers) {
        this.headers = headers;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = mapHeaders(chain);
        return chain.proceed(request);
    }

    private Request mapHeaders(Interceptor.Chain chain) {
        Request original = chain.request();

        Request.Builder requestBuilder = original.newBuilder();

        for (Map.Entry<String, String> entry : headers.entrySet()) {
            requestBuilder.addHeader(entry.getKey(), entry.getValue());
        }

        return requestBuilder.build();
    }
}
