package com.lutfi.cermatirecuitmenttest.core.base;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Lutfi on 1/24/2020.
 */
public abstract class BaseActivity extends AppCompatActivity implements IBaseNavigator {

    private Unbinder mUnBinder;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(getLayoutResource());
        setUnbinder(ButterKnife.bind(this));
        onViewReady();
    }

    @Override
    protected void onDestroy() {
        if (mUnBinder != null) {
            try {
                mUnBinder.unbind();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }

        super.onDestroy();
    }

    private void setUnbinder(Unbinder unbinder) {
        this.mUnBinder = unbinder;
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        IBinder iBinder = null;
        try {
            iBinder = getCurrentFocus().getWindowToken();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        if (iBinder != null) {
            inputManager.hideSoftInputFromWindow(iBinder,
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void onViewReady() {
        initLib();
        initIntent();
        initUI();
        initAction();
        initProcess();
    }

    //    pass Layout
    protected abstract int getLayoutResource();

    //    Init Lib
    protected abstract void initLib();

    //    Extract intent here
    protected abstract void initIntent();

    //    init UI, setup toolbar, setText etc here
    protected abstract void initUI();

    //    init UI interaction here
    protected abstract void initAction();

    //    init main Process here
    protected abstract void initProcess();
}
