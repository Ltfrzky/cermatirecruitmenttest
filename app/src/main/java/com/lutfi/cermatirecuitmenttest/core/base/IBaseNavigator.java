package com.lutfi.cermatirecuitmenttest.core.base;

/**
 * Created by Lutfi on 1/24/2020.
 */
public interface IBaseNavigator {

    void showToast(String message);

    void finishActivity();

    void hideKeyboard();
}
