package com.lutfi.cermatirecuitmenttest.screens.main;

import android.content.Context;

import com.lutfi.cermatirecuitmenttest.R;
import com.lutfi.cermatirecuitmenttest.data.api.libs.ApiConnection;
import com.lutfi.cermatirecuitmenttest.data.api.model.error.ErrorResponse;
import com.lutfi.cermatirecuitmenttest.data.api.model.response.SearchResponse;
import com.lutfi.cermatirecuitmenttest.util.ContextProvider;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lutfi on 1/24/2020.
 */
public class MainViewModel {
    MainNavigator navigator;

    private int LIMIT = 20;

    private int PAGE = 1;

    private boolean isLoadMore = false;

    public MainViewModel(MainNavigator navigator) {
        this.navigator = navigator;
    }

    public void setLoadMore(boolean loadMore) {
        isLoadMore = loadMore;
    }

    public boolean isLoadMore() {
        return isLoadMore;
    }

    void reset() {
        PAGE = 1;
    }

    void searchUser(String keyword, boolean isLoadMore) {
        this.isLoadMore = isLoadMore;
        if (isLoadMore) {
            navigator.onLoadmoreStart();
            PAGE += 1;
        } else {
            reset();
            navigator.showLoading();
        }

        ApiConnection.searchUser(keyword, PAGE, LIMIT).enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                if (response.isSuccessful()) {
                    if (isLoadMore) {
                        setLoadMore(false);
                        navigator.onLoadmoreComplete();
                    } else {
                        navigator.hideLoading();
                    }
                    if (response.body().getTotalCount() > 0) {
                        navigator.searchUserSuccess(response.body().getUserList());
                    }else{
                        navigator.searchUserFailed(ContextProvider.get().getString(R.string.error_no_result), false);
                    }
                } else {
                    navigator.hideLoading();
                    if(response.code() == 403){
                        navigator.searchUserFailed(ContextProvider.get().getString(R.string.error_too_much_request), true);
                    }else{
                        navigator.searchUserFailed(ErrorResponse.getErrorMessage(response.errorBody()), false);
                    }
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                if (isLoadMore) {
                    setLoadMore(false);
                    navigator.searchUserFailed(t.getMessage(), false);
                    navigator.onLoadmoreEnd();
                } else {
                    navigator.hideLoading();
                }
                navigator.searchUserFailed(t.getMessage(), false);
            }
        });
    }
}
