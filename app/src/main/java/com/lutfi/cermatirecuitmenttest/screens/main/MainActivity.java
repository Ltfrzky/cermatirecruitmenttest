package com.lutfi.cermatirecuitmenttest.screens.main;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.lutfi.cermatirecuitmenttest.R;
import com.lutfi.cermatirecuitmenttest.core.adapter.BaseRecyclerAdapter;
import com.lutfi.cermatirecuitmenttest.core.base.BaseActivity;
import com.lutfi.cermatirecuitmenttest.data.api.model.User;
import com.lutfi.cermatirecuitmenttest.screens.adapter.UserAdapter;
import com.lutfi.cermatirecuitmenttest.util.ContextProvider;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainNavigator, UserAdapter.OnLoadMoreListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;
    @BindView(R.id.rv_search)
    RecyclerView rvSearch;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.sv_main)
    SearchView svMain;

    private MainViewModel viewModel;

    private String keyword = "";

    private UserAdapter userAdapter;

    private LinearLayoutManager linearLayoutManager;

    private List<User> userList;

    public static void start(Context context) {
        Intent starter = new Intent(context, MainActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void initLib() {
        ContextProvider.initialize(this);
        viewModel = new MainViewModel(this);
    }

    @Override
    protected void initIntent() {

    }

    @Override
    protected void initUI() {
        linearLayoutManager = new LinearLayoutManager(this);

        userAdapter = new UserAdapter(this, new ArrayList<User>());
        userAdapter.setLinearLayoutManager(linearLayoutManager);
        userAdapter.setOnLoadMoreListener(this);
        userAdapter.setRecyclerView(rvSearch);

        rvSearch.setHasFixedSize(true);
        rvSearch.setLayoutManager(linearLayoutManager);
        rvSearch.setAdapter(userAdapter);

    }

    @Override
    protected void initAction() {
        userAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                showToast(userAdapter.getDatas().get(position).getLogin());
            }
        });

        svMain.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!query.isEmpty()) {
                    if (userList != null) {
                        userList.clear();
                        userAdapter.clear();
                    }
                    keyword = query;
                    searchUser(keyword);
                    return true;
                } else {
                    showToast("No Keyword");
                    return true;
                }
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    @Override
    protected void initProcess() {

    }

    private void searchUser(String keyword) {
        userAdapter.setLoadmoreProgress(false);
        viewModel.searchUser(keyword, false);
    }

    @Override
    public void onLoadmoreStart() {
        userAdapter.setLoadmoreProgress(true);
    }

    @Override
    public void onLoadmoreComplete() {
        userAdapter.setLoadmoreProgress(false);
    }

    @Override
    public void onLoadmoreEnd() {
        userAdapter.removeScrollListener();
    }

    @Override
    public void searchUserSuccess(List<User> userList) {
        this.userList = userList;
        userAdapter.addOrUpdate(userList);
        rvSearch.setVisibility(View.VISIBLE);
        hideKeyboard();
    }

    @Override
    public void searchUserFailed(String message, boolean isLoadMore) {
        if (isLoadMore) {
            showToast(message);
            userAdapter.setLoadmoreProgress(false);
        } else {
            showToast(message);
            tvError.setVisibility(View.VISIBLE);
            pbLoading.setVisibility(View.GONE);
            userAdapter.setLoadmoreProgress(false);
        }
    }

    @Override
    public void onLoadMore() {
        viewModel.searchUser(keyword, true);
    }

    @Override
    public void showLoading() {
        tvError.setVisibility(View.GONE);
        pbLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        pbLoading.setVisibility(View.GONE);
    }
}
