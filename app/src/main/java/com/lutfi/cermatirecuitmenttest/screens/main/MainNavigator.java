package com.lutfi.cermatirecuitmenttest.screens.main;

import com.lutfi.cermatirecuitmenttest.core.base.IBaseNavigator;
import com.lutfi.cermatirecuitmenttest.data.api.model.User;

import java.util.List;

/**
 * Created by Lutfi on 1/24/2020.
 */
public interface MainNavigator extends IBaseNavigator {

    void showLoading();

    void hideLoading();

    void onLoadmoreStart();

    void onLoadmoreComplete();

    void onLoadmoreEnd();

    void searchUserSuccess(List<User> userList);

    void searchUserFailed(String message, boolean isLoadMore);
}
