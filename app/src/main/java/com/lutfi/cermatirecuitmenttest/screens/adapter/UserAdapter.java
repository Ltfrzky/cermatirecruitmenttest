package com.lutfi.cermatirecuitmenttest.screens.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.lutfi.cermatirecuitmenttest.R;
import com.lutfi.cermatirecuitmenttest.core.adapter.BaseItemViewHolder;
import com.lutfi.cermatirecuitmenttest.core.adapter.BaseRecyclerAdapter;
import com.lutfi.cermatirecuitmenttest.data.api.model.User;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Lutfi on 1/24/2020.
 */
public class UserAdapter extends BaseRecyclerAdapter<User, BaseItemViewHolder> {

    private final int VIEW_ITEM = 1;

    private final int VIEW_LOADMORE = 0;

    private boolean isMoreLoading = false;

    private int visibleThreshold = 0;

    private int lastVisibleItem, totalItemCount;

    private LinearLayoutManager linearLayoutManager;

    private OnLoadMoreListener onLoadMoreListener;

    private RecyclerView recyclerView;

    private RecyclerView.OnScrollListener scrollListener;

    @Override
    public int getItemViewType(int position) {
        return getDatas().get(position) != null ? VIEW_ITEM : VIEW_LOADMORE;
    }

    public UserAdapter(Context context) {
        super(context);
    }

    public UserAdapter(Context context, List<User> users) {
        super(context, users);
    }

    @Override
    protected int getItemResourceLayout(int viewType) {
        int view = 0;

        if (viewType == VIEW_ITEM) {
            view = R.layout.item_user;
        } else if (viewType == VIEW_LOADMORE) {
            view = R.layout.item_loadmore;
        }

        return view;
    }

    @Override
    public BaseItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM) {
            return new UserViewHolder(mContext, getView(parent, viewType), mItemClickListener, mLongItemClickListener);
        } else {
            return new LoadmoreViewholder(mContext, getView(parent, viewType), mItemClickListener, mLongItemClickListener);
        }
    }

    public class UserViewHolder extends BaseItemViewHolder<User> {

        @BindView(R.id.img_user)
        ImageView imgUser;
        @BindView(R.id.tv_name)
        TextView tvName;

        public UserViewHolder(Context mContext, View itemView, OnItemClickListener itemClickListener, OnLongItemClickListener longItemClickListener) {
            super(mContext, itemView, itemClickListener, longItemClickListener);
        }

        @Override
        public void bind(User user) {
            Glide.with(mContext).load(user.getAvatarUrl()).into(imgUser);
            tvName.setText(user.getLogin());
        }
    }

    public class LoadmoreViewholder extends BaseItemViewHolder<User> {

        @BindView(R.id.progressBar)
        ProgressBar progressBar;

        public LoadmoreViewholder(Context mContext, View itemView, OnItemClickListener itemClickListener, OnLongItemClickListener longItemClickListener) {
            super(mContext, itemView, itemClickListener, longItemClickListener);
        }

        @Override
        public void bind(User user) {
            if (isMoreLoading) {
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    public void setLoadmoreProgress(final boolean isProgress) {
        isMoreLoading = isProgress;
        if (isProgress) {
            getDatas().add(getDatas().size(), null);
            notifyDataSetChanged();
        } else {
            if (getDatas().size() > 0) {
                getDatas().remove(getDatas().size() - 1);
                notifyDataSetChanged();
            }
        }
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public OnLoadMoreListener getOnLoadMoreListener() {
        return onLoadMoreListener;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public LinearLayoutManager getLinearLayoutManager() {
        return linearLayoutManager;
    }

    public void setLinearLayoutManager(LinearLayoutManager linearLayoutManager) {
        this.linearLayoutManager = linearLayoutManager;
    }

    public void setRecyclerView(RecyclerView recyclerView) {

        this.recyclerView = recyclerView;

        scrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = getLinearLayoutManager().getItemCount();
                lastVisibleItem = getLinearLayoutManager().findLastVisibleItemPosition() + 1;
                if (!isMoreLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (onLoadMoreListener != null) {
                        getOnLoadMoreListener().onLoadMore();
                        isMoreLoading = true;
                    }
                }
            }
        };

        recyclerView.addOnScrollListener(scrollListener);
    }

    public void removeScrollListener() {
        recyclerView.removeOnScrollListener(scrollListener);
    }
}
